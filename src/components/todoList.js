import React, {Component} from 'react';
import {connect} from 'react-redux';
import TodoItem from './todoItem';
import {addTodo} from '../actions/todoActions';
 
class TodoList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title : '',
        }
        
        this.addTodo = this.addTodo.bind(this);
            
    }
 
    addTodo() {

        if(this.state.title !== '' && !this.props.todos.some(elem => elem.title === this.state.title)) {

            this.props.add(this.state.title);

            this.refs.todoName.value = '';

            this.setState({title: ''})
        }
        else if (this.state.title === '') {
            alert("Введите задачу")
        }
        else {
            alert("Такая задача уже есть")
        };
    }

    render() {
        return (
            <div className="todoList">
                <div className="todoItems">
                    <h2>Задачи</h2>
                    {this.props.todos.map(todo => <TodoItem key={todo.title} title={todo.title} />)}
                </div>
                <div className="todoNew">
                    <h2>Новая задача</h2>
                    <textarea className="todoNewInput" ref="todoName" id="textInput" type="text" onChange={e => this.setState({title: e.target.value})} />
                    <button className="todoNewButton" onClick={this.addTodo}>Добавить задачу</button>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        todos: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        add: (title) => dispatch(addTodo(title))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(TodoList);