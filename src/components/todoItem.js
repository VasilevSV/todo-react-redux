import React, {Component} from 'react'

class TodoItem extends Component {

    render() {
        return (
            <div className="todoItem">
                <div className="todoName">
                    <p>{this.props.title}</p>
                </div>
                <div className="completeSwitch">
                    <label className="complete">Выполнено
                    <input type="checkbox" />
                    <span className="checkmark"></span></label>
                </div>
            </div>
        );
    }


}

export default TodoItem;