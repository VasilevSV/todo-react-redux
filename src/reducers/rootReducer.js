import { ADD_TODO} from "../constants";

let initialState = [{title:'Выучить Реакт'}, {title:'Выучить Редакс'}, {title:'Покормить Марси'}]

let todoReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_TODO:
            return [...state, {title:action.payLoad.title,}]
        default:
            return state;
    }
}

export default todoReducer;