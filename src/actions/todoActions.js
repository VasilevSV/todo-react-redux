import {ADD_TODO} from '../constants'

export let addTodo = (title = 'Выучить Редакс') => {
    return {
        type: ADD_TODO,
        payLoad: {
            title
        }
    }
}
