import React, {Component} from 'react'
import {Provider} from 'react-redux'
import TodoList from './components/todoList'
import store from './store'
import './styles/style.css';

class App extends Component {
    render() {
        return (
            <main>
                <Provider store={store}>
                    <div className="container">
                        <TodoList />
                    </div>
                </Provider>
            </main>
        );
    }


}


export default App;